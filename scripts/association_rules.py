import pandas as pd
from  mlxtend import frequent_patterns as fp
from  mlxtend import preprocessing

def format_data(
                df,
                categories
                ):
    """
    Function that takes data from a dataframe and formats it for input into the
    apriori algorithm

    Parameters:
        df (pd.DataFrame):            DataFrame containing the data to format
        categories (list of strings): list of columns from df to include in
                                      formatted data

    Returns:
        profiles (list of lists):     list containing lists of desired
                                      information for each candidate
    """
    #Create list to store lists of candidate information
    profiles = []
    #Loop through each candidate entry in the df
    for i in range(len(df)):
        #Create list to store candidate information
        profile = []

        #For each input category
        for cat in categories:
            #Check if category is valid
            if cat not in df.columns:
                raise KeyError(cat + ' is not a column in the dataframe')

            #Store category value
            val = df[cat][i]
            #Get type of category value
            val_type = type(val)

            #Add value to the profile list
            if val_type != list:
                #Keep NoneType objects from being added to the list
                if val is not None:
                    profile += [val]
                else:
                    pass
            else:
                #Keep NoneType objects from being added to the list
                while None in val:
                    val.remove(None)
                profile += val

        #Append profile list to list of profile lists
        profiles.append(list(profile))

    return profiles


def generate_itemset(
                     profiles,
                     min_support=.5,
                     max_length=5
                     ):
    """
    Function that generates a set of the most frequently appearing items using
    the apriori algorithm

    Parameters:
        profiles (list of lists):       list generated by the format_data function
        min_support (float):            float representing the minimum support an
                                        item must have to qualify as frequent

    Returns:
        frequent_itemset (pd.DataFrame): df containing the frequent items
                                             and their supports
    """
    #Create TransactionEncoder object for reformatting the profiles
    print('Encoding transaction into sparse matrix')
    te = preprocessing.TransactionEncoder()

    #Reformat the profiles and store them in a dataframe
    value_matrix = te.fit(profiles).transform(profiles, sparse=True)
    value_matrix = pd.SparseDataFrame(value_matrix, columns=te.columns_)

    #Generate set of frequent items using apriori
    print('Generating itemset for value matrix of shape:', value_matrix.shape)
    return fp.apriori(value_matrix, min_support, verbose = 1,
                      max_len = max_length, use_colnames=True)


def generate_rules(
                   df,
                   categories,
                   min_support=.5,
                   max_length=5,
                   metric='confidence',
                   min_threshold=.5,
                   ):
    """
    Function that uses the format_data and generate_itemsets to generate a list
    of association rules for the input dataframe

    Parameters:
        df (pd.DataFrame): DataFrame containing the data to format
        categories (list of strings): list of columns from df to include in
                                          formatted data
        min_support (float):          float representing the minimum support
                                          an item must have to qualify as frequent
        max_length (int):
        metric (string):              metric to include/exclude association rules
                                      on. Options are:
                                        'support', 'confidence', 'lift',
                                        'leverage', and 'conviction'
        min_threshold (float):        minimum value metric must meet to be
                                      included in the association rules

    Returns:
        rules (pd.DataFrame): df containing the association rules
    """
    #Format data
    print('Formatting data for dataframe of size:', df.shape)
    formatted_data = format_data(
                                 df,
                                 categories
                                 )

    #Get frequent itemset
    frequent_itemsets = generate_itemset(
                                         formatted_data,
                                         min_support
                                         )

    #Generate rules
    print('Generating rules for itemset of size:', frequent_itemsets.shape)
    rules = fp.association_rules(
                                 frequent_itemsets,
                                 metric=metric,
                                 min_threshold=min_threshold
                                 )

    print('Completed')
    return rules
