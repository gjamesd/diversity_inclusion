import pandas as pd
from datetime import datetime
from dateutil import relativedelta
from dateutil.relativedelta import *
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import numpy as np
import nltk
import csv
import pandas as pd
import numpy as np
import pandas as pd
import nltk
#import spacy
from scipy.spatial import distance
from nltk.corpus import stopwords
from collections import Counter
import pickle
import sys
import os
import io
from io import BytesIO
#from docx import Document
#import seaborn as sns
#from statsmodels.stats.proportion import proportions_ztest
from scipy import stats
from scipy.stats import ttest_ind
from scipy import spatial
import json
from sklearn.decomposition import PCA
import nltk
from nltk.corpus import stopwords
import glob
from collections import Counter
from tqdm import tqdm
import warnings
import itertools
#import tensorflow as tf  # !pip install "tensorflow>=2.0.0"
#import tensorflow_hub as hub  # !pip install "tensorflow_hub>=0.6.0"
from uszipcode import SearchEngine
warnings.simplefilter('ignore')

global job_req
global orderboard_profile_df
global client_company_name
global job_req_vector
global company_norm_dict
global max_title_level
global abbys_skills
global abbys_languages
global abbys_large_datasets
global abbys_visualization
global normal_model_path
global job_req_titles
global job_req_skills
global job_req_info
global job_req_education
global job_req_experience_time

temp_df = pd.read_csv('/Users/a13855/Desktop/OrderBoard/div_inc/data/normalized_companies_master.csv', encoding='latin1')
company_norm_dict = {key: value for key, value in zip(temp_df['Company'].tolist(), temp_df['Desired Name'].tolist())}

min_title_level = 1
max_title_level = 2000

analysis_date = datetime.strptime(datetime.today().strftime("%Y-%m-%d"), '%Y-%m-%d')


orderboard_profile_df = pd.read_csv('/Users/a13855/Desktop/OrderBoard/div_inc/data/Orderboard_Profile_Info-3.csv', encoding='latin1')
#normal_model_path = '/Users/home/Desktop/Gitlab/OBDataSci/company-normalization-project/nnlm-en-dim128_2'
compensation_model_path = '/Users/a13855/Desktop/OrderBoard/div_inc/files/cpi_adjusted3.pkl'
#fred_org_df = pd.read_excel(open('/Users/home/Desktop/Gitlab/OBDataSci/company-normalization-project/Freds_Titles and Durations.xlsx', 'rb'), sheet_name='Title')


def clean_company_name(x):
    if x in company_norm_dict.keys():
        return company_norm_dict[x]
    else:
        return x


def parseDate(dateString):
    for dateFormat in ['%Y-%m-%d', '%Y-%m', '%Y']:
        try:
            return datetime.strptime(dateString, dateFormat)
        except ValueError:
            continue
    raise ValueError("No valid date format found for " + dateString)


def get_average_job_time(x):
    '''
    parameters : x - row from the dataframe

    return : average time at all jobs
    '''
    ls = []
    # for experience in the experince list
    try:
        for exper in x['experience']:
            # if we have a start and end date then there should be a length in months already calculated
            if exper['start_date'] is not None and exper['end_date'] is not None:
                try:
                    ls.append(exper['experience_length_months'])
                # If it's not there then we do other options
                except:
                    datetime_object_start = parseDate(exper['start_date'])
                    datetime_object_end = parseDate(exper['end_date'])
                    ls.append(((datetime_object_end.year - datetime_object_start.year) * 12) + datetime_object_end.month - datetime_object_start.month)
        if len(ls) == 0:
            return 0
        return sum(ls) / len(ls)
    except:
        return 0


def get_list_of_past_companies(x):
    list_companies_and_date = []
    # Figure it out yourself
    for exper in x['experience']:
        try:
            if exper['company']['name'] is not None and exper['start_date'] is not None:
                list_companies_and_date.append([clean_company_name(exper['company']['name']), parseDate(exper['start_date'])])
        except:
            pass
    list_companies_and_date.sort(key=lambda x: x[1])
    list_companies = [i[0] for i in list_companies_and_date]
    return [i for n, i in enumerate(list_companies) if i not in list_companies[:n]]


def get_primary_name(x):
    # should be pretty obvious
    try:
        return x['primary']['name']['clean']
    except:
        return 'Unknown'


def get_previous_company(x):
    company = 'Unknown'
    time_since = np.inf
    # Here we keep track of the smalled time since and the company, finding the last job
    try:
        x['experience']
    except:
        return 'Unkown'
    for exper in x['experience']:
        if exper['end_date'] is not None:
            datetime_object = parseDate(exper['end_date'])
            today = datetime.today()
            if ((today.year - datetime_object.year) * 12) + today.month - datetime_object.month < time_since:
                try:
                    if exper['company'] is not None and clean_company_name(exper['company']['name']) != clean_company_name(x['primary']['job']['company']['name']):
                            company = clean_company_name(exper['company']['name'])
                            time_since = ((today.year - datetime_object.year) * 12) + today.month - datetime_object.month
                except:
                    pass
            # print(((today_year - datetime_object.year) * 12) + today_month - datetime_object.month)
    # print(company, time_since)
    return company


def get_current_company(x):
    # Being lazy
    try:
        return clean_company_name(x['primary']['job']['company']['name'])
    except:
        return 'Unknown'


def get_primary_industry(x):
    # Again
    try:
        return x['primary']['industry']
    except:
        return 'Unknown'


def get_primary_profile(x):
    try:
        return x['primary']['linkedin']
    except:
        return 'Unknown'


def get_skills_list(x):
    skill_ls = []
    try:
        for skill in x['skills']:
            skill_ls.append(skill['name'])
        return skill_ls
    except:
        return skill_ls


def get_certifications(x):
    certifications_ls = []
    for cert in x['certifications']:
        certifications_ls.append(cert['name'])
    return certifications_ls


def get_geo_location(x):
    try:
        return x['orderboard']['geo_location']
    except:
        try:
            for local in x['locations']:
                if local['is_primary']:
                    return local['geo']
        except:
            return None


def complete_summary(x):
    summary_ls = []
    # Here we get the summary from each individual experience along with the main profile summary
    # And put them all together in one big string
    for exper in x['experience']:
        for item in exper['summaries']:
            summary_ls.append(item)
    for part in x['summaries']:
        summary_ls.append(part)
    # print(summary_ls)
    full_summary = ' '.join(summary_ls)
    return full_summary


def get_primary_job_title(x):
    try:
        return x['primary']['job']['title']['name']
    except:
        return 'Unknown'


def get_time_in_chair(x):
    # it works
    if x['primary']['job'] is None:
        return None
    elif x['primary']['job']['start_date'] is None:
        return None
    else:
        datetime_object = parseDate(x['primary']['job']['start_date'])
        today = analysis_date
        return ((today.year - datetime_object.year) * 12) + today.month - datetime_object.month


def get_NLP_summary(x):
    # Here we concatenate the important text from the candidate into a giant string for the NLP comparison
    return x['complete_summary'] + 'My skills include ' + ' '.join(x['skills_ls']) + ' My certifications are ' + ' '.join(x['certifications_ls']) + ' My Job titles are ' + ' '.join(x['job_title_ls']) + ' My Majors are ' + ' '.join(x['majors_ls']) + ' My Degreesnare ' + ' '.join(x['degrees_ls'])


def get_majors(x):
    major_ls = []
    for edu in x['education']:
        for major in edu['majors']:
            major_ls.append(major)
    return major_ls


def get_degrees(x):
    degree_ls = []
    for edu in x['education']:
        for degree in edu['degrees']:
            degree_ls.append(degree)
    return degree_ls


def get_job_title_ls(x):
    job_title_ls = []
    for exper in x['experience']:
        try:
            job_title_ls.append(exper['title']['name'])
        except:
            pass
    return job_title_ls


def convert_to_percentage(x):
    try:
        return int(x * 100)
    except:
        return 0


temp_df = pd.read_csv('/Users/a13855/Desktop/OrderBoard/div_inc/data/industry_classifier_normalized.csv', encoding='unicode_escape')
industry_dictionary = {key: value for key, value in zip(temp_df['Company'].tolist(), temp_df['Industry'].tolist())}


def get_inferred_industry(df):
    # this is to do inferred industry from the list that robin has been building

    def get_industry(x):
        if x in industry_dictionary.keys():
            return industry_dictionary[x]
        else:
            return 'Unknown'

    tqdm.pandas(desc='Getting Inferred Industry')
    df['inferred_industry'] = df['current_company'].progress_apply(lambda x: get_industry(x))
    return df


def get_orderboard_inferred_industry(x, pdl_industry):
    if x in industry_dictionary.keys():
        return industry_dictionary[x]
    elif pdl_industry is not None:
        return pdl_industry
    else:
        return 'Unknown'


def get_orderboard_diversity_inclusion(df):
    # Diversity and inclusion is something that's for JnJ
    # these lists are maually created by Dean
    diversity_inclusion_dict_id = {key: value for key, value in zip(orderboard_profile_df['id'].tolist(), orderboard_profile_df['diversity'].tolist())}
    diversity_inclusion_dict_linkedin = {key: value for key, value in zip(orderboard_profile_df['linkedin'].tolist(), orderboard_profile_df['diversity'].tolist())}

    def get_diversity_inclusion(x):
        if x['id'] in diversity_inclusion_dict_id.keys():
            if diversity_inclusion_dict_id[x['id']] == 'yes' or diversity_inclusion_dict_id[x['id']] == 'no':
                return diversity_inclusion_dict_id[x['id']]
            elif x['linkedin'] in diversity_inclusion_dict_linkedin.keys():
                if diversity_inclusion_dict_linkedin[x['linkedin']] == 'yes' or diversity_inclusion_dict_linkedin[x['linkedin']] == 'no':
                    return diversity_inclusion_dict_linkedin[x['linkedin']]
            return 'Unknown'
        elif x['linkedin'] in diversity_inclusion_dict_linkedin.keys():
            if diversity_inclusion_dict_linkedin[x['linkedin']] == 'yes' or diversity_inclusion_dict_linkedin[x['linkedin']] == 'no':
                return diversity_inclusion_dict_linkedin[x['linkedin']]
        return 'Unknown'
    tqdm.pandas(desc='Getting Diversity Inclusion')
    df['diversity_inclusion'] = df.progress_apply(lambda x: get_diversity_inclusion(x), axis=1)
    return df


def get_orderboard_gender(df):
    # Similar to the diversity and inclusion, Dean went through and varified some people
    orderboard_gender_dict_id = {key: value for key, value in zip(orderboard_profile_df['id'].tolist(), orderboard_profile_df['Gender'].tolist())}
    orderboard_gender_dict_linkedin = {key: value for key, value in zip(orderboard_profile_df['linkedin'].tolist(), orderboard_profile_df['Gender'].tolist())}

    def get_gender(x):
        if x['id'] in orderboard_gender_dict_id.keys():
            if orderboard_gender_dict_id[x['id']] == 'male' or orderboard_gender_dict_id[x['id']] == 'female':
                return orderboard_gender_dict_id[x['id']]
            elif x['linkedin'] in orderboard_gender_dict_linkedin.keys():
                if orderboard_gender_dict_linkedin[x['linkedin']] == 'male' or orderboard_gender_dict_linkedin[x['linkedin']] == 'female':
                    return orderboard_gender_dict_linkedin[x['linkedin']]
                elif x['gender'] is not None:
                    return x['gender']
                else:
                    return 'Unknown'
            elif x['gender'] is not None:
                return x['gender']
            else:
                return 'Unknown'
        elif x['linkedin'] in orderboard_gender_dict_linkedin.keys():
            if orderboard_gender_dict_linkedin[x['linkedin']] == 'male' or orderboard_gender_dict_linkedin[x['linkedin']] == 'female':
                return orderboard_gender_dict_linkedin[x['linkedin']]
            elif x['gender'] is not None:
                return x['gender']
            else:
                return 'Unknown'
        elif x['gender'] is not None:
            return x['gender']
        else:
            return 'Unknown'
    tqdm.pandas(desc='Getting Orderboard Gender')
    df['orderboard_gender'] = df.progress_apply(lambda x: get_gender(x), axis=1)
    return df


def get_orderboard_ethnicity(df):
    orderboard_ethnicity_dict_id = {key: value for key, value in zip(orderboard_profile_df['id'].tolist(), orderboard_profile_df['ethnicity'].tolist())}
    orderboard_ethnicity_dict_linkedin = {key: value for key, value in zip(orderboard_profile_df['linkedin'].tolist(), orderboard_profile_df['ethnicity'].tolist())}

    def get_ethnicity(x):
        if x['id'] in orderboard_ethnicity_dict_id.keys():
            return orderboard_ethnicity_dict_id[x['id']]
        if x['linkedin'] in orderboard_ethnicity_dict_linkedin.keys():
            return orderboard_ethnicity_dict_linkedin[x['linkedin']]
        return 'Unknown'

    tqdm.pandas(desc='Getting Orderboard Ethnicity')
    df['orderboard_ethnicity'] = df.progress_apply(lambda x: get_ethnicity(x), axis=1)
    return df


def get_clean_company(df):
    # This is to normalize company names thanks to Jay
    tqdm.pandas(desc='Cleaning Company Names')
    df['clean_company'] = df['current_company'].progress_apply(lambda x: get_company(x))
    return df


def get_candidate_locality(x):
    # This is getting the Locality. Eventually we would want this replaced with MSA
    try:
        return x['primary']['location']['locality']
    except:
        return 'Unknown'


def get_candidate_region(x):
    try:
        return x['primary']['location']['region']
    except:
        return 'unknown'

'''
def get_specified_candidates(df):
    #from geopy.distance import great_circle
    mapping_dictionary_df = pd.read_csv('/Users/home/Desktop/Gitlab/OBDataSci/company-normalization-project/Map_Location_dictionary.csv')
    mapping_dictionary_id = {key: value for key, value in zip(mapping_dictionary_df['Candidate Document ID'].tolist(), mapping_dictionary_df['Map_Locality'].tolist())}
    mapping_dictionary_linkedin = {key: value for key, value in zip(mapping_dictionary_df['Candidates Primary Linkedin URL'].tolist(), mapping_dictionary_df['Map_Locality'].tolist())}
    city_geo_dict_df = pd.read_csv('/Users/home/Desktop/Gitlab/OBDataSci/company-normalization-project/city_geo_coordinates.csv')
    city_geo_dict = {key: value for key, value in zip(city_geo_dict_df['local'].tolist(), city_geo_dict_df['geo'].tolist())}

    ls_cities = [('Washington, District of Columbia', 50)]

    def locate(x):
        for city in ls_cities:
            if great_circle(x['geo_location'], city_geo_dict[city[0]]).miles < city[1]:
                return city[0]
        if x['id'] in mapping_dictionary_id.keys():
            return mapping_dictionary_id[x['id']]
        if x['linkedin'] in mapping_dictionary_linkedin.keys():
            return mapping_dictionary_linkedin[x['linkedin']]
        return 'Unknown'

    tqdm.pandas(desc='Getting Specified Locations')
    df['Map_location'] = df.progress_apply(lambda x: locate(x), axis=1)
    return df
'''

def get_title_level(df, in_column_name='job_title', out_column_name='title_level', show_progress=True):
    dic = {'intern': 0,
            'internship': 0,
            'apprenticeship': 0,
            'board ': 11,
            'ceo ': 10,
            'executive data scientist': 5,
            'executive ': 10,
            'exec ': 10,
            'cto ': 9,
            'cfo ': 9,
            'chief ': 9,
            'pres ': 9,
            'president ': 9,
            'svp ': 9,
            'vp ': 8,
            'vice president ': 8,
            'general manager ': 7,
            'director ': 6,
            'director,': 6,
            'dir ': 6,
            'head ': 6,
            'principal ': 5,
            'senior associate ': 2,
            'sr. associate ': 2,
            'staff ': 4,
            'senior manager': 4,
            'senior ': 3,
            'sr. ': 3,
            'manager ': 3,
            ' manager': 3,
            'manager, ': 3,
            'lead ': 2,
            'associate ': 1,
            'sde 3': 3,
            'sde 2': 2,
            'sde 1': 1,
            'l3': 3,
            'l2': 2,
            'l1': 1,
            'ic4': 4,
            'ic3': 3,
            'ic2': 2,
            'mts4': 4,
            'mts3': 3,
            'mts2': 2,
            'mts1': 1,
            '5': 5,
            '4': 4,
            '3': 3,
            ' iii': 3,
            '2': 2,
            ' ii': 2,
            '-ii': 2,
            '1': 1}

    def get_title_level(x):
        if x is None:
            return 0
        for key in dic:
            if key in x:
                # print(f'{x} : {key} : {dic[key]}')
                return dic[key]
        else:
            return 1

    if show_progress:
        tqdm.pandas(desc='Getting Title Level')
        df[out_column_name] = df[in_column_name].progress_apply(lambda x: get_title_level(x))
    else:
        df[out_column_name] = df[in_column_name].apply(lambda x: get_title_level(x))
    return df


def timeDiff(datetime_object_start, datetime_object_end):
    # getting time difference
    return (datetime_object_end.year - datetime_object_start.year) * 12 + datetime_object_end.month - datetime_object_start.month


def timeSince(dateString, defaultReturn=np.inf):
    try:
        datetime_object = parseDate(dateString)
    except:
        return defaultReturn
    return timeDiff(datetime_object, analysis_date)


def get_overdue_data(x):
    unkownComps = 0
    compTimes = {'unkown': np.inf}
    for job in x['experience']:
        try:
            comp = job['company']['name']
            if comp not in compTimes.keys():
                compTimes[comp] = -1
            for date in [job['start_date'], job['end_date']]:
                if (date is not None):
                    if timeSince(date, -1) > compTimes[comp]:
                        compTimes[comp] = timeSince(date, -1)
        except:  # comp = 'unkown'
            unkownComps += 1
            for date in [job['start_date'], job['end_date']]:
                if date is not None:
                    if timeSince(date) < compTimes['unkown']:
                        compTimes['unkown'] = timeSince(date)
                        break
    curCompTime = min([time if time > -1 else np.inf for time in compTimes.values()])
    numComps = len(compTimes) - 1 + unkownComps  # -1 for the key 'unkown'
    if curCompTime == np.inf:
        curCompTime = 0
    try:
        return (x['extrapolated_career_length_months'], curCompTime, numComps)
    except:
        return (0, curCompTime, numComps)


def filter_time_in_comp(x):
    careerLen, curCompTime, numComps = get_overdue_data(x)
    if numComps == 0:
        return True
    return (curCompTime >= careerLen / numComps) or (curCompTime <= 12)


def get_time_at_company(x):
    # Finding the time at current company for Jay's Addressable analysis
    return get_overdue_data(x)[1]


def get_normal_matrix(df):
    print('Loading Normal Model')
    embed = hub.load(normal_model_path)
    cosine_loss = tf.keras.losses.CosineSimilarity(axis=1, reduction=tf.keras.losses.Reduction.SUM)
    cos_sim = tf.keras.metrics.CosineSimilarity(axis=1)
    kld = tf.keras.metrics.KLDivergence()
    poisson = tf.keras.metrics.Poisson()
    cat_hinge = tf.keras.metrics.CategoricalHinge()
    hinge = tf.keras.metrics.Hinge()
    log_cosh = tf.keras.metrics.LogCoshError()

    def get_normal_list(x):
        job_title_text = x['job_title']
        skills_certs_text = ' '.join(x['skills_ls']) + ' ' + ' '.join(x['certifications_ls'])
        job_info_text = x['complete_summary']
        education_text = ' '.join(x['majors_ls']) + ' ' + ' '.join(x['degrees_ls'])
        job_experience_time = str(np.log(x['inferred_years_experience'])) + ' years of experience '

        return [job_title_text, skills_certs_text, job_info_text, education_text, job_experience_time]

    def cosine_sim(target, x, weights=[1, 1, 1, 1, 1]):
        cos_sim.reset_states()
        _ = cos_sim.update_state(target, x, sample_weight=weights)
        return cos_sim.result().numpy()

    def kld_sim(target, x, weights=[1, 1, 1, 1, 1]):
        kld.reset_states()
        _ = kld.update_state(target, x, sample_weight=weights)
        return kld.result().numpy()

    def poisson_sim(target, x, weights=[1, 1, 1, 1, 1]):
        poisson.reset_states()
        _ = poisson.update_state(target, x, sample_weight=weights)
        return poisson.result().numpy()

    def cat_hinge_sim(target, x, weights=[1, 1, 1, 1, 1]):
        cat_hinge.reset_states()
        _ = cat_hinge.update_state(target, x, sample_weight=weights)
        return cat_hinge.result().numpy()

    def hinge_sim(target, x, weights=[1, 1, 1, 1, 1]):
        hinge.reset_states()
        _ = hinge.update_state(target, x, sample_weight=weights)
        return hinge.result().numpy()

    def log_cosh_sim(target, x, weights=[1, 1, 1, 1, 1]):
        log_cosh.reset_states()
        _ = log_cosh.update_state(target, x, sample_weight=weights)
        return log_cosh.result().numpy()

    tqdm.pandas(desc='Creating list for normal similarity')
    df['normal_list'] = df.progress_apply(lambda x: get_normal_list(x), axis=1)
    tqdm.pandas(desc='Creating embedding for normal similarity')
    df['normal_embedding'] = df['normal_list'].progress_apply(lambda x: embed(x).numpy())
    return df


# --------------Total Compensation Data----------------- #
# importing random forest infastructure
global DS_regressor
with open(compensation_model_path, 'rb') as file:
    DS_regressor = pickle.load(file)


def base_pay_prediction_DS(x):
    base_data_scientist_ratio = .684275
    senior_data_scientist_ratio = .625334
    staff_data_scientist_ratio = .554445
    principle_data_scientist_ratio = .6084783
    distinguished_fellow_data_scientist_ratio = .648409
    max_base_salary = 350000

    # Base Data Scientist
    if x['title_level'] == 1:
        return x['predicted_sal'] * base_data_scientist_ratio
    # Senior Data Scientist
    elif x['title_level'] in [2, 3]:
        return x['predicted_sal'] * senior_data_scientist_ratio
    # Staff Data Scientist
    elif x['title_level'] == 4:
        return x['predicted_sal'] * staff_data_scientist_ratio
    # Principle Data Scientist
    elif x['title_level'] == 5:
        if x['predicted_sal'] * principle_data_scientist_ratio < max_base_salary:
            return x['predicted_sal'] * principle_data_scientist_ratio
        else:
            return max_base_salary
    # Distinguished fellow and above
    else:
        if x['predicted_sal'] * distinguished_fellow_data_scientist_ratio < max_base_salary:
            return x['predicted_sal'] * distinguished_fellow_data_scientist_ratio
        else:
            return max_base_salary


def total_compensation_predication(df):
    DS_comps_keep = ['microsoft', 'amazon', 'facebook', 'uber', 'google', 'apple', 'capital one']
    big_tech_list = ['amazon', 'amazon web services', 'apple', 'facebook', 'google', 'ibm', 'linkedin', 'microsoft', 'netflix', 'twitter', 'uber']

    working = df.copy()
    # Create the bool columns for each company necessary for the model
    for company in DS_comps_keep:
        if company not in working.columns.tolist():
            working[company] = working['current_company'].apply(lambda x: 1 if x == company else 0)
    working['DS_company_other'] = df['current_company'].apply(lambda x: 1 if x not in DS_comps_keep else 0)

    # Set the title for each candidate for the model
    working['Data Scientist'] = working['title_level'].apply(lambda x: int(x == 1))
    working['Senior Data Scientist'] = working['title_level'].apply(lambda x: 1 if x in [2, 3] else 0)
    working['Staff Data Scientist'] = working['title_level'].apply(lambda x: 1 if x in [4] else 0)
    working['Principal Data Scientist'] = working['title_level'].apply(lambda x: 1 if x in [5] else 0)
    working['Distinguished Fellow'] = working['title_level'].apply(lambda x: 1 if x in [6] else 0)
    working['inferred_years_experience'].fillna((working['inferred_years_experience'].mean()), inplace=True)
    # Create the consumer price index dictionary
    cl = pd.read_csv("/Users/a13855/Desktop/OrderBoard/div_inc/data/cost_of_living.csv")
    cl['State'] = cl.State.str.lower()
    cost_of_living_dict = {key: value for key, value in zip(cl['State'].tolist(), cl['costIndex'].tolist())}
    # Append the relevant CPI for each person
    working['cpi'] = working['region'].apply(lambda x: cost_of_living_dict[x] / 100 if x is not None else 1)
    df['cpi'] = df['region'].apply(lambda x: cost_of_living_dict[x] / 100 if x is not None else 1)
    # Check if they work at a big tech company
    working['big_tech'] = working['current_company'].apply(lambda x: x in big_tech_list)

    def DS_predict(x):
        try:
            to_predict = x[['years_at_company', 'inferred_years_experience', 'cpi', 'big_tech', 'microsoft', 'amazon', 'facebook', 'uber', 'google', 'apple', 'capital one', 'DS_company_other', 'Data Scientist', 'Distinguished Fellow', 'Principal Data Scientist', 'Senior Data Scientist', 'Staff Data Scientist']]
            to_predict = pd.DataFrame(to_predict).transpose()
            return DS_regressor.predict(to_predict)[0]
        except:
            print(pd.DataFrame(x).transpose().columns.tolist())
            print(x[['years_at_company', 'inferred_years_experience', 'cpi', 'big_tech', 'microsoft', 'amazon', 'facebook', 'uber', 'google', 'apple', 'capital one', 'DS_company_other', 'Data Scientist', 'Distinguished Fellow', 'Principal Data Scientist', 'Senior Data Scientist', 'Staff Data Scientist']])

    def DS_predict_multiple(df):
        try:
            return DS_regressor.predict(df)
        except:
            print(df)

    tqdm.pandas(desc='Predicting Salaries')
    # df['predicted_sal'] = working.progress_apply(lambda x: np.e**(DS_predict(x)), axis=1)
    df['predicted_sal'] = DS_predict_multiple(working[['years_at_company', 'inferred_years_experience', 'cpi', 'big_tech', 'microsoft', 'amazon', 'facebook', 'uber', 'google', 'apple', 'capital one', 'DS_company_other', 'Data Scientist', 'Distinguished Fellow', 'Principal Data Scientist', 'Senior Data Scientist', 'Staff Data Scientist']])

    tqdm.pandas(desc='Adjusting Compensation for Cost of Living')
    df['predicted_sal'] = df.progress_apply(lambda x: x['predicted_sal'] / (x['cpi']), axis=1)
    tqdm.pandas(desc='Getting Base Pay Prediction')
    df['predicted_base'] = df.progress_apply(lambda x: base_pay_prediction_DS(x), axis=1)
    tqdm.pandas(desc='Getting Incentive Prediction')
    df['predicted_incentive'] = df.progress_apply(lambda x: x['predicted_sal'] - x['predicted_base'], axis=1)

    return df


# ---------------Done-------------------- #
#this is for candidate history 
search = SearchEngine(simple_zipcode=True)
world_geo_df = pd.read_csv('/Users/a13855/Desktop/OrderBoard/div_inc/data/World Country Geo.csv')
world_geo_dict = {key: value for key, value in zip(world_geo_df.name.tolist(), world_geo_df.geo.tolist())}
US_State_list = ['alabama', 'alaska', 'arizona', 'arkansas', 'california', 'colorado', 'connecticut', 'delaware', 'florida', 'georgia', 'hawaii', 'idaho', 'illinois', 'indiana', 'iowa', 'kansas', 'kentucky', 'louisiana', 'maine', 'maryland', 'massachusetts', 'michigan', 'minnesota', 'mississippi', 'missouri', 'montana', 'nebraska', 'nevada', 'new hampshire', 'new jersey', 'new mexico', 'new york', 'north carolina', 'north dakota', 'ohio', 'oklahoma', 'oregon', 'pennsylvania', 'rhode island', 'south carolina', 'south dakota', 'tennessee', 'texas', 'utah', 'vermont', 'virginia', 'washington', 'west virginia', 'wisconsin', 'wyoming', 'district of columbia']
US_city_state_geo = pd.read_csv('/Users/a13855/Desktop/OrderBoard/div_inc/data/US City_State Geo.csv')
US_city_state_geo_dict = {key: value for key, value in zip(US_city_state_geo.city_state.tolist(), US_city_state_geo.geo.tolist())}


def candidate_history_analsis(x):

    def interpret_correct_geo_coords(x):
        if x['geo'] is not None and x['locality'] is not None and x['region'] is not None and x['location'] is not None:
            return x
        if x['geo'] is None and x['locality'] is None and x['region'] is None and x['location'] is None:
            return x
        if x['location'] is not None:
            ls1 = x['location'].split(',')
            # print(ls1)
            if len(ls1) == 3:
                if ls1[2].strip() == 'united states':
                    x['locality'] = ls1[0].strip()
                    x['region'] = ls1[1].strip()
                else:
                    x['locality'] = ls1[1].strip()
                    x['region'] = ls1[2].strip()
            elif len(ls1) == 2:
                x['locality'] = ls1[0].strip()
                x['region'] = ls1[1].strip()
            elif len(ls1) == 1:
                x['region'] = ls1[0].strip()
            else:
                raise ValueError(f'{ls1} is a weird length')
        if x['locality'] is not None and x['region'] is not None:
            x['location'] = f'{x["locality"]}, {x["region"]}'
        if x['geo'] is None:
            if x['region'] in US_State_list:
                try:
                    if x['locality'] is None:
                        x['locality'] = 'None'
                    if 'st.' in x["locality"]:
                        x["locality"] = x["locality"].replace("st.", "saint")
                    x['geo'] = US_city_state_geo_dict[f'{x["locality"]}, {x["region"]}']
                    # raise ValueError(f"Didn't find results for <<{x['locality']}, {x['region']}\n{x[['start_date', 'job_title', 'company', 'end_date', 'exp_len', 'locality', 'region', 'location', 'geo']]}>>")
                except:
                    try:
                        res = search.by_city_and_state(x["locality"], x["region"])
                        if len(res) > 0:
                            x['geo'] = f"{res[0].lat}, {res[0].lng}"
                            US_city_state_geo_dict.update({f'{x["locality"]}, {x["region"]}': x['geo']})
                    except:
                        pass
                    #  print(f'<<{x["locality"]}, {x["region"]}>> was not in the US geo dictionary')
                    pass
            else:
                if x['region'] in world_geo_dict.keys():
                    x['geo'] = world_geo_dict[x['region']]
        return x

    data = {'start_date': [],
            'job_title': [],
            'company': [],
            'desc': [],
            'primary_bool': [],
            'end_date': [],
            'exp_len': [],
            'work_exp_bool': [],
            'location': [],
            'locality': [],
            'region': [],
            'geo': [],
            'unemployment': [],
            'experience_type': [],
            'industry': []}

    for edu in x['education']:
        start_date = pd.NaT  # Done
        job_title = None  # Done
        company = None  # Done
        desc = ''  # Done
        primary_bool = False  # Done
        end_date = pd.NaT  # Done
        exp_len = None  # Done
        work_exp_bool = False  # Done
        location = None
        locality = None
        region = None
        geo = None
        unemployment = False
        experience_type = 'Academic'
        industry = None
        try:
            # Actual Experience filter
            if edu['end_date'] is None and edu['start_date'] is None:
                continue
            # End Date Analysis
            if edu['end_date'] is None:
                end_date = pd.NaT
            else:
                end_date = parseDate(edu['end_date'])
            # Start Date Analysis
            if edu['start_date'] is None:
                start_date = pd.NaT
            else:
                start_date = parseDate(edu['start_date'])

            # School Name
            if edu['school']['name'] is not None:
                company = clean_company_name(edu['school']['name'])
            else:
                company = 'Unknown School'

            # Job Title - What type of school experience is this
            # High school experience
            degs_str = ' '.join(edu['degrees'])
            if edu['school']['type'] == 'secondary school':
                job_title = 'High School Experience'
                industry = 'High School Experience'
            # University school - Figure out if it's a Masters, Bachelors, or PHD. Default should be masters
            elif 'phd' in degs_str or 'doctorates' in degs_str:
                job_title = 'PhD. Program'
                industry = 'PhD. Program'
            elif 'masters' in degs_str:
                job_title = 'Masters Degree Experience'
                industry = 'Masters Degree Experience'
            elif 'bachelor' in degs_str:
                job_title = 'Bachelors Degree Experience'
                industry = 'Bachelors Degree Experience'
            else:
                # print(f'Unkown Degree Experience in <<{degs_str}>>')
                job_title = 'University/College Experience'
                industry = 'University/College Experience'
            # Experience Length
            if start_date is not pd.NaT and end_date is not pd.NaT:
                exp_len = relativedelta(end_date, start_date).months
            if start_date is pd.NaT and end_date is not pd.NaT:
                start_date = end_date - relativedelta(years=+4)

            # Get Location
            location = edu['school']['location']

            # Description
            desc = f"Going to {company} for a {job_title} from {start_date} to {end_date}.\nMajors include : {' '.join(edu['majors'])}.\nDegrees include : {degs_str}.\nSummaries : {' '.join(edu['summaries'])}.\nRaw date : {' '.join(edu['raw'])} "

            data['start_date'].append(start_date)
            data['job_title'].append(job_title)
            data['company'].append(company)
            data['desc'].append(desc)
            data['primary_bool'].append(primary_bool)
            data['end_date'].append(end_date)
            data['exp_len'].append(exp_len)
            data['work_exp_bool'].append(work_exp_bool)
            data['location'].append(location)
            data['locality'].append(locality)
            data['region'].append(region)
            data['geo'].append(geo)
            data['unemployment'].append(unemployment)
            data['experience_type'].append(experience_type)
            data['industry'].append(industry)
        except Exception as e:
            # print("Failed School Experience")
            # print(e)
            # print(edu)
            continue

    for exper in x['experience']:
        start_date = pd.NaT  # Done
        job_title = None
        company = None
        desc = ''
        primary_bool = False  # Done
        end_date = pd.NaT  # Done
        exp_len = None  # Done
        work_exp_bool = True
        location = None
        locality = None
        region = None
        geo = None
        unemployment = False
        experience_type = 'Unknown'
        industry = None
        try:
            # Actual Experience filter
            if exper['end_date'] is None and exper['start_date'] is None:
                if exper['is_primary']:
                    end_date = parseDate(datetime.today().strftime("%Y-%m-%d"))
                else:
                    continue
            # End Date Analysis
            if exper['end_date'] is None:
                if exper['is_primary']:
                    end_date = parseDate(datetime.today().strftime("%Y-%m-%d"))
                else:
                    end_date = pd.NaT
            else:
                end_date = parseDate(exper['end_date'])
            # Start Date Analysis
            if exper['start_date'] is None:
                start_date = pd.NaT
            else:
                start_date = parseDate(exper['start_date'])

            # Is Primary
            primary_bool = exper['is_primary']

            # Experience length
            try:
                exp_len = exper["experience_length_months"]
            except:
                exp_len = 0

            # Job Title, Company cleaned
            if exper['title'] is None:
                job_title = 'Unknown'
            else:
                job_title = exper['title']['name']
            if exper['company'] is None:
                continue
            company = clean_company_name(exper['company']['name'])
            industry = exper['company']['industry']
            industry = get_orderboard_inferred_industry(company, industry)
            experience_type = 'Professional' if industry in ['Consulting', 'cpg', 'Hi-tech (hardware)', 'Hi-tech (software)', 'HI-tech (software)', 'Consumer and Packaged Goods', 'Professional Services', 'Financial Services', 'Government', 'Healthcare', 'Manufacturing', 'Retail/Personal Services', 'Medicine', 'Entertainment', 'Energy', 'Conglomerates', 'Nonprofit', 'Conglomerate', 'Construction'] else ('Academic' if industry in ['Academic', 'University', 'Education'] else 'Unknown')
            # Get Location information
            if len(exper['locations']) > 0:
                locality = exper['locations'][0]['locality']
                region = exper['locations'][0]['region']
                geo = exper['locations'][0]['geo']

            desc = f"Working for {company} as a {job_title} in {location} from {start_date} to {end_date}.\nExperience time is {exp_len}.\nsummaries : {' '.join(exper['summaries'])}"

            data['start_date'].append(start_date)
            data['job_title'].append(job_title)
            data['company'].append(company)
            data['desc'].append(desc)
            data['primary_bool'].append(primary_bool)
            data['end_date'].append(end_date)
            data['exp_len'].append(exp_len)
            data['work_exp_bool'].append(work_exp_bool)
            data['location'].append(location)
            data['locality'].append(locality)
            data['region'].append(region)
            data['geo'].append(geo)
            data['unemployment'].append(unemployment)
            data['experience_type'].append(experience_type)
            data['industry'].append(industry)

        except:
            print(exper)
            print("Unexpected error:", sys.exc_info()[0])
            raise

    # print(x['linkedin'], x['id'])
    history_df = pd.DataFrame.from_dict(data)
    history_df = history_df.apply(lambda x: interpret_correct_geo_coords(x), axis=1)
    # print(history_df[['start_date', 'end_date', 'job_title', 'company', 'desc']])
    if len(history_df) == 1:
        if history_df.iloc[0]['end_date'] is None:
            history_df.at[0, 'end_date'] = parseDate(datetime.today().strftime("%Y-%m-%d"))
    history_df.drop_duplicates(inplace=True)
    history_df.sort_values(by='start_date', inplace=True, ignore_index=True)

    # Cleaning Start Date for Primary Job
    for pos in range(len(history_df)):
        if history_df.iloc[pos]['primary_bool'] and history_df.iloc[pos]['start_date'] is pd.NaT:
            found = False
            end_time = history_df.iloc[pos]['end_date']
            pos_ = pos
            while pos_ != 0 and not found:
                pos_ -= 1
                if history_df.iloc[pos_]['end_date'] < end_time:
                    history_df.at[pos, 'start_date'] = history_df.iloc[pos_]['end_date']
                    found = True
                    break

    end_date_list = history_df['end_date'].tolist()
    start_date_list = history_df['start_date'].tolist()
    for pos in range(len(history_df)):
        if isinstance(end_date_list[pos], pd._libs.tslibs.nattype.NaTType):
            company = history_df.iloc[pos]['company']
            for pos2 in range(pos + 1, len(history_df)):
                if history_df.iloc[pos2]['company'] == company and not isinstance(start_date_list[pos2], pd._libs.tslibs.nattype.NaTType):
                    history_df.at[pos, 'end_date'] = start_date_list[pos2]
                    break

    history_df['exp_len'] = history_df.apply(lambda x: abs(relativedelta(x['end_date'], x['start_date']).months) + abs((12 * relativedelta(x['end_date'], x['start_date']).years)) if x['end_date'] is not pd.NaT and x['start_date'] is not pd.NaT else 0, axis=1)
    date_list = history_df['start_date'].tolist() + history_df['end_date'].tolist()
    delta = relativedelta(months=+1)
    date_tup_list = []
    history_df.apply(lambda x: date_tup_list.append((x['start_date'], x['end_date'], x['job_title'], x['company'], x['location'], x['locality'], x['region'], x['geo'])), axis=1)
    try:
        if len(history_df) > 1:
            curr_time = parseDate(date_list[0].strftime("%Y-%m")) + delta
            stop_point = parseDate(datetime.today().strftime("%Y-%m"))
            miss_list = []
            while curr_time < stop_point:
                covered = False
                for i in range(len(date_tup_list)):
                    tup = date_tup_list[i]
                    if i != 0:
                        prev_tup = date_tup_list[i - 1]
                    else:
                        prev_tup = (None, None, None, None)
                    if tup[0] <= curr_time <= tup[1]:
                        covered = True
                        if len(miss_list) == 0:
                            break
                        else:
                            # print(f"Need to append row for time period {miss_list[0]} - {tup[0]}")
                            len_of_addition = abs(relativedelta(tup[0], miss_list[0]).months) + abs((12 * relativedelta(tup[0], miss_list[0]).years))
                            if len_of_addition <= 12:  # Transition Period
                                job_title = f"Transition - {prev_tup[2]}"
                                company = f"Transition - {prev_tup[3]}"
                                desc = f"Transitioning jobs.\nPreviously worked at {prev_tup[3]} as a {prev_tup[2]}.\nTransition length is {len_of_addition} months"
                            else:
                                job_title = f"Unemployed - {prev_tup[2]}"
                                company = f"Unemployed - {prev_tup[3]}"
                                desc = f"Unemployed.\nPreviously worked at {prev_tup[3]} as a {prev_tup[2]}.\nUnemployment length is {len_of_addition} months"
                            df_to_add = pd.DataFrame.from_dict({'start_date': [miss_list[0]], 'job_title': [job_title], 'company': [company], 'desc': [desc], 'primary_bool': [False], 'end_date': [tup[0]], 'exp_len': [len_of_addition], 'work_exp_bool': [False], 'location': [prev_tup[4]], 'locality': [prev_tup[5]], 'region': [prev_tup[6]], 'geo': [prev_tup[7]], 'unemployment': [True], 'experience_type': ['Unemployed'], 'industry': ['Unemployed']})
                            history_df = pd.concat([history_df, df_to_add], ignore_index=True)
                            miss_list = []
                            break
                if not covered:
                    miss_list.append(curr_time)
                curr_time = curr_time + delta
        history_df.sort_values(by='start_date', inplace=True)
    except Exception as e:
        print(e)
        print(x['education'])
        print(x['experience'])
        print(history_df)
        input()
    history_df = get_title_level(history_df, in_column_name='job_title', out_column_name='job_title_level', show_progress=False)
    history_df['start_date'] = history_df['start_date'].apply(lambda x: x.strftime("%Y-%m-%d") if x is not pd.NaT else None)
    history_df['end_date'] = history_df['end_date'].apply(lambda x: x.strftime("%Y-%m-%d") if x is not pd.NaT else None)
    return history_df.to_dict()


#fred_org_dict = {key: value for key, value in zip(fred_org_df['Title'].tolist(), fred_org_df['Organization'].tolist())}


def get_fred_organization(x):
    try:
        return fred_org_dict[x]
    except KeyError:
        return 'Unknown'
    except:
        raise


def json_clean(df):
    '''
    Parameters : df - Dataframe that consistes of only the core json file from passelBot

    output : df - Dataframe with the necessary columns to do grouping
    '''
    tqdm.pandas(desc='Getting average job time')
    df['avg_job_time'] = df.progress_apply(lambda x: get_average_job_time(x), axis=1)
    tqdm.pandas(desc='Getting past companies')
    df['past_companies'] = df.progress_apply(lambda x: get_list_of_past_companies(x), axis=1)
    tqdm.pandas(desc='Getting name')
    df['name'] = df.progress_apply(lambda x: get_primary_name(x), axis=1)
    tqdm.pandas(desc='Getting previous company')
    df['previous_company'] = df.progress_apply(lambda x: get_previous_company(x), axis=1)
    tqdm.pandas(desc='Getting current_company')
    df['current_company'] = df.progress_apply(lambda x: get_current_company(x), axis=1)
    tqdm.pandas(desc='Getting primary industry')
    df['primary_industry'] = df.progress_apply(lambda x: get_primary_industry(x), axis=1)
    tqdm.pandas(desc='Getting linkedin')
    df['linkedin'] = df.progress_apply(lambda x: get_primary_profile(x), axis=1)
    tqdm.pandas(desc='Getting skills list')
    df['skills_ls'] = df.progress_apply(lambda x: get_skills_list(x), axis=1)
    tqdm.pandas(desc='Getting certifications list')
    df['certifications_ls'] = df.progress_apply(lambda x: get_certifications(x), axis=1)
    tqdm.pandas(desc='Getting geo location')
    df['geo_location'] = df.progress_apply(lambda x: get_geo_location(x), axis=1)
    tqdm.pandas(desc='Getting complete summary')
    df['complete_summary'] = df.progress_apply(lambda x: complete_summary(x), axis=1)
    tqdm.pandas(desc='Getting time at current company')
    df['months_current_company'] = df.progress_apply(lambda x: get_time_at_company(x), axis=1)
    tqdm.pandas(desc='Getting years at company')
    df['years_at_company'] = df['months_current_company'].progress_apply(lambda x: x / 12)
    tqdm.pandas(desc='Checking inferred years experience')
    df['inferred_years_experience'] = df.progress_apply(lambda x: x['inferred_years_experience'] if x['inferred_years_experience'] > x['years_at_company'] else x['years_at_company'], axis=1)
    tqdm.pandas(desc='Getting job title')
    df['job_title'] = df.progress_apply(lambda x: get_primary_job_title(x), axis=1)
    tqdm.pandas(desc='Getting time in chair')
    df['time_in_chair'] = df.progress_apply(lambda x: get_time_in_chair(x), axis=1)
    tqdm.pandas(desc='Getting majors')
    df['majors_ls'] = df.progress_apply(lambda x: get_majors(x), axis=1)
    tqdm.pandas(desc='Getting degrees')
    df['degrees_ls'] = df.progress_apply(lambda x: get_degrees(x), axis=1)
    tqdm.pandas(desc='Getting job title list')
    df['job_title_ls'] = df.progress_apply(lambda x: get_job_title_ls(x), axis=1)
    tqdm.pandas(desc='Getting locality')
    df['locality'] = df.progress_apply(lambda x: get_candidate_locality(x), axis=1)
    tqdm.pandas(desc='Getting regions')
    df['region'] = df.progress_apply(lambda x: get_candidate_region(x), axis=1)
    #tqdm.pandas(desc="Gettings Fred's Organization")
    #df['Organization_Fred'] = df['job_title'].progress_apply(lambda x: get_fred_organization(x))
    tqdm.pandas(desc="Getting Fred's Level")
    df['level_Fred'] = df['extrapolated_career_length_months'].progress_apply(lambda x: 1 if x < 97 else (2 if x < 201 else (3 if x < 291 else (4 if x < 384 else 5))))
    tqdm.pandas(desc='Getting NLP Summary')
    df['NLP_summary'] = df.progress_apply(lambda x: get_NLP_summary(x), axis=1)
    df = get_title_level(df)
    # Total Compensation
    #df = total_compensation_predication(df)
    # Total Compensation Done
    print(f'Dropping candidates outside of {min_title_level} - {max_title_level} range')
    # df2 = df[df['title_level'] <= max_title_level]
    # df1 = df2[df2['title_level'] >= min_title_level]
    df1 = df
    df1.reset_index(inplace=True)
    tqdm.pandas(desc='Performing addressable supply - Ben')
    df1['comp_length_filter'] = df1.progress_apply(filter_time_in_comp, axis=1)
    #print('Creating Addressable')
    #df1['addressable'] = df1[['title_level', 'comp_length_filter']].all(axis='columns')
    df1 = get_inferred_industry(df1)
    #df1 = get_orderboard_diversity_inclusion(df1)
    #df1 = get_orderboard_gender(df1)
    #df1 = get_orderboard_ethnicity(df1)
    #df1 = get_specified_candidates(df1)
    # get time at company
    tqdm.pandas(desc='Creating candidate history info')
    df1['candidate_history'] = df1.progress_apply(lambda x: candidate_history_analsis(x), axis=1)
   #df1 = get_normal_matrix(df1)
    return df1


def main():  # '/Users/home/Desktop/Passel Json Files/JnJ - TS DS - fit1 (v2).xlsx'\
    input_path_ls = ['/Users/a13855/Desktop/OrderBoard/div_inc/data/diversity_and_inclusion_part_1-revisionId-568.ndjson']
    base_export_name_ls = ['/Users/a13855/Desktop/OrderBoard/div_inc/data/cleaned_div_inc.ndjson']
   
    for input_, base_ in zip(input_path_ls, base_export_name_ls):
        # Read in the Passel output .json or .ndjson
        try:
            df = pd.read_json(input_, lines=False)
        except:
            df = pd.read_json(input_, lines=True)

        # Clean the df from the json
        print(input_)
        df = json_clean(df)

        df.to_json(base_, orient='records', lines=True)



if __name__ == '__main__':
    main()
