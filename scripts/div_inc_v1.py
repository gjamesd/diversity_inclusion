#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Diversity and Inclusion Model, James and Ben! 
"""

### Import 
import pandas as pd
import numpy as np
import matplotlib.pyplot as  plt
import seaborn as sns 
import warnings
from mlxtend import preprocessing
import association_rules as ar
warnings.filterwarnings("ignore")


# =============================================================================
'''
using some of the functions we used for components to see the support, and see if
I can easily make dummy columns out of some common results 
'''

def get_col_type(df, col):
    vals = df[col].to_list()
    col_type = None
    for item in vals:
        if item is None:
            pass
        else:
            col_type = type(item)
            break
    return col_type
def drop_not_top(df, col, to_keep):
    """
    Function that deletes items from columns in dataframe that don't exist in the 'to_keep' list
    Parameters
    ----------
    df      (pd.DataFrame): Dataframe containing column col 
    col     (str)         : Column that you want to clean
    to_keep (list)        : List containing only values you want to keep in col 
    Returns
    -------
    df (pd.DataFrame): Dataframe with col only containing values from to_keep 
    """
    col_type = get_col_type(df,col)

    total_skills_list = []
    
    if col_type == str:
        for candidate in df[col]:
            if candidate in to_keep:
                total_skills_list.append(candidate)
            else:
                total_skills_list.append([])
                
    elif col_type == list:            
        for candidate in df[col]:
            if candidate is not None:
                total_skills_list.append([skill for skill in candidate if skill in to_keep])
            else:
                total_skills_list.append([])
    else:
        raise ValueError('dtype of this column cannot be handled at this time')
                
    df['dropped_{}'.format(col)] = total_skills_list
    return df
def flatten(l):
    """
    Function that flattens a list of lists
    Parameters
    ----------
    l (list): List of lists to flatten
    Returns
    -------
    newl (list): Flattened list
    """
    # variable to store flattened list
    newl = []
    if l is not None:
        # Loop through every sublist
        for sublist in l:
            # Check for NoneTypes
            if sublist is not None:
                # Append every entry in sublist to single flat list
                for item in sublist:
                    newl.append(item)
                    
    return newl

def breakout_cols(df, col,num_skills):
    #Reformat the column
    
    previous_format = ar.format_data(df, [col])
    #Encode column into booelan matrix
    #Create transaction encoder
    te = preprocessing.TransactionEncoder()
    #Fit transaction encoder with previous jobs
    encoded_matrix = te.fit(previous_format).transform(previous_format, sparse=False) 
    #Encode te into matrix
    encoded_matrix = pd.DataFrame(encoded_matrix, columns=te.columns_)              

    #Now format matrix correctly
    
    #Ensure all values are numerical
    encoded_matrix = 1*encoded_matrix.fillna(0).transpose()
    #Get total numbers of occurences of each job
    total = encoded_matrix.sum(axis=1)   
    #Get total number of unique jobs present in the matrix
    total_previous = encoded_matrix.shape[0]
    #Add total jobs column
    encoded_matrix['total'] = total 
    
    #Trim down matrix to only jobs reaching a certain support
    support = encoded_matrix.total/encoded_matrix.shape[1]
    encoded_matrix['support'] = support
    encoded_matrix = encoded_matrix.sort_values(by='support', ascending=False)

    return encoded_matrix[:num_skills]
# =============================================================================

### read in labeled data
#labeled = pd.read_csv('../data/candidate_diversity_inclusion.csv')

### Read in data from PDL 
data = pd.read_json('/Users/a13855/Desktop/OrderBoard/div_inc/data/cleaned_div_inc.ndjson', lines = True)

l_info = pd.read_json("/Users/a13855/Desktop/OrderBoard/div_inc/data/labeled_div_inc.json", lines = True)
l_labels = pd.read_csv("/Users/a13855/Desktop/OrderBoard/div_inc/data/Orderboard_Profile_Info.csv")



#taking columns I don't want out 

data2 = data[['career_count', 'education', 'gender','birth_date', 'inferred_years_experience',
                 'experience', 'certifications_ls', 'career_length_months',
                 'skills_ls', 'names', 'name', 'industries', 'primary_industry', 'locations', 'avg_job_time', 
                 'complete_summary', 'job_title', 'job_title_ls', 'majors_ls', "degrees_ls",
                 'region', 'locality', 'candidate_history']]

def get_labels(df1, df2):
    #need to get the columns that I want, all in one location and combine the data from pdl
    #with the hand crafted labels that steven and co went through and did 
    
    #first getting rid of columns from the labels that I don't need 
    df1 = df1.drop(columns = ['Unnamed: 5', 'Unnamed: 6', 'Unnamed: 7',
       'Unnamed: 8'], axis = 1)
    #now merge 
    df3 = pd.merge(df1, df2, left_on = 'id', right_on = 'id')
    
    return df3

labels = get_labels(l_labels, l_info)
#trying to get all of the same columns for the labeled data set as treatment 

def get_certifications(df):
    def actual(x):
        certifications_ls = []
        for cert in x['certifications']:
            certifications_ls.append(cert['name'])
        return certifications_ls
    df['certifications_ls'] = df.apply(lambda x: actual(x), axis=1)
    return df

def get_interests(df):
    def actual(x):
        interests_ls = []
        for passion in x['interests']:
            interests_ls.append(passion['name'])
        return interests_ls
    df['interests_ls'] = df.apply(lambda x: actual(x), axis=1)
    return df



data3 = data2[data2.index <= 15000]
print(len(data3))
# =============================================================================
'''
Ok so  it is my hypothesis that we can use the information from the columns selected,
and manipulate them in such a way that we can predict ethnicity and gender, ok 
lets get after it 
'''
### UNDERSTANDING DATA
'''
a lof of the data that we will use comes from the candidate_history column, within  
that column we have info on their start dates, job titles, companies, summaries, 
experiencel lengths, locations of cities and states, industries. 
'''

# =============================================================================

def get_past_countries(x):
    #function to pull out countries of each person where x is the location feild for 
    #each person 
    return [x[pos]['country'] for pos in range(len(x))]


def get_degrees(df):
    def actual(x):
        degree_ls = []
        for edu in x['education']:
            if len(edu['degrees']) > 0:
                degree_ls.append(edu['degrees'][0].split()[0])
        return list(set(degree_ls))
    df['degrees_ls'] = df.apply(lambda x: actual(x), axis=1)
    return df

def prev_countries(df):
    #creating the past companies column
    df['past_countries'] = df['locations'].apply(lambda x: get_past_countries(x))
    
    #creating a list of dictionaries for each category of country and the prodominant race 
    countries = {
        'asian':['china', 'india', 'south korea', 'japan',
                       'vietnam', 'thailand', 'indonesia', 'laos',
                       'singapore', 'hong kong', 'phillipines', 'malaysia',
                       'cambodia', 'taiwan', 'bangladesh', 'mongolia', 'napal',
                       'brunei', 'sri lanka', 'maldives', 'north korea'],
           
           'pacific':['samoa', 'tonga', 'fiji', 'tahi', 'guam', 'new zealand',
                      'united states minor outlying islands', ],
           
           'latin':['belize', 'costa rica', 'el salvador', 'guatemala', 'honduras',
                    'mexico', 'nicaragua', 'panama', 
                    'argentina', 'bolivia', 'brazil', 'chile', 'colombia', 'ecuador',
                    'french guiana', 'guyana', 'uruguay', 'venezuela', 'paraguay', 
                    'cuba', 'dominican republic', 'puerto rico', 
                    ],
           
           'african':['haiti', 'jamaca', 'trinidad and tobago', 'nigeria', 'etheopia',
                    'democratic republic of the congo', 'congo', 'tanzania',
                    'south africa', 'sudan', 'angola', 'mozambique', 'ghana', 
                    'madagascar', 'cameroon', "côte d'ivorie", 'ivory coast',
                    "cote d'ivorie", 'cote divoire', 'côte divoire', 'niger', 
                    'burkina faso', 'mali', 'zambia', 'malawi', 'senegal', 'chad',
                    'zimbabwe', 'rwanada', 'wakanda', 'benin', 'sierra leone', 
                    'congo', 'central african republic', 'central african republic of the congo',
                    'namibia', 'gambia', 'botswana',
                    ],
           
           #not including the  'united states', 'united states of america' because it will not be an indicator 
           'european':['canada', 
                    'scotland', 'wales', 'ireland', 'england', 'united kingdom', 
                    'italy', 'france', 'germany', 'spain', 'portugal', 'austria',
                    'belgium', 'bulgaria', 'croatia', 'czech republic', 'denmark',
                    'estonia', 'finland', 'greece', 'hungary', 'latvia', 'lithuania',
                    'luxembourg', 'malta', 'netherlands', 'poland', 'romania',
                    'slovakia', 'slovenia', 'sweden', 'isreal', 'turkey', 'syria',
                    'iran', 'jordan', 'lebanon', 'afganistan', 'uzbekistan', 
                    'libya', 'saudi arabia', 'united arab emirates', 'russia',
                    'albania', 'algeria', 'andorra', 'croatia', 'czechia', 'iraq',
                    'latvia', 'montenegro', 'monaco', 'netherlands', 'australia', 
                    'serbia', 'turkey', 'united arab emirates', 'ukraine', 
                    'saudi arabia'],
           
           }
    
    #creating a data frame with every possible country in someones history         
    c = pd.get_dummies(df.past_countries.apply(pd.Series).stack()).sum(level = 0)
    
    #grouping thosfe countries baseed on their group 
    df['asian'] = c.apply(lambda x: sum([x[country] for country in list(set(countries['asian']) & set(c.columns.tolist()))]), axis = 1)
    df['european'] = c.apply(lambda x: sum([x[country] for country in list(set(countries['european']) & set(c.columns.tolist()))]), axis = 1)
    df['pacific'] = c.apply(lambda x: sum([x[country] for country in list(set(countries['pacific']) & set(c.columns.tolist()))]), axis = 1)
    df['latin'] = c.apply(lambda x: sum([x[country] for country in list(set(countries['latin']) & set(c.columns.tolist()))]), axis = 1)
    df['african'] = c.apply(lambda x: sum([x[country] for country in list(set(countries['african']) & set(c.columns.tolist()))]), axis = 1) 
    
    #Come back and fix this so they are dummies, and not conitniuous 
    
    return df 
 
def get_edu_level(df):
    #getting the possible degrees for each person
    l = pd.get_dummies(df.degrees_ls.apply(pd.Series).stack()).sum(level = 0)
    #normalizing 
    b = l.bachelor + l.bachelors
    m = l.master + l.masters
    phd = l.doctor + l.doctorates
    
    df['associates'] = l.associates
    df['bachelors'] = b
    df['masters'] = m
    df['phd'] = phd
    return df 
    
def get_len_summary(df):
    '''it is my hypothesis that women will write about their work experiences
    more than men, so I think... think that their summaries might be longer than
    thosee of men, also it might be significant with some minority groups? so 
    making a function to get that 
    '''
    df['sum_length'] = df.complete_summary.apply(lambda x: len(x))
    return df 

def get_pdl_gender(df):
    df['pdl_male'] = df.gender.apply(lambda x: 1 if x == 'male' else 0)
    df['pdl_female'] = df.gender.apply(lambda x: 1 if x == 'female' else 0)
    df['no_pdl_gender'] = df.gender.apply(lambda x: 1 if x == None else 0)
    return df 


def get_major_dicipline(df):
    #creating a function to map majors  to a dicipline 
    diciplines = {
        'business':['business','business admin','business administration',
                              'business administration and management', 'business economics',
                              'business finance','business law','business management',
                              'business management and administration','business marketing',
                              'business studies','marketing', 'human resources', 'strateegy', "finance",
                             'business strateegy', 'information systems',
                             'business', 'marketing', 'information systems management',
                             'international business', 'international finance', 
                             'occupational health', 'experience design',
                             'finance', 'finance and marketing','financial accounting',
                             'financial economics','financial engineering',
                             'financial forensics and fraud investigation',
                             'financial management', 'consulting','corporate finance',
                             'entrepreneurial studies', 'entrepreneurship','general business',
                             'human relations','human resource development','human resource management',
                             'human resources','human resources management','human services',
                             'information science','information tech','international business',
                             'international commerce', 'international finance','logistics',
                             'product marketing', 'recreation administration','supply chain management',
                             'baking','banking','accountancy','accounting',
                              
                              ],
                  
                
                  
                  'arts':['jazz','jazz performance', 'violin', 'music', 'viola', 'cello', 'trumpet',
                          'bass', 'saxaphone', 'commercial art','ceramics', 'conducting',
                          'dance', 'dance performance', 'design',  'digital art',
                          'digital video','digital video and cinema','drama', 'drawing',
                          'masonry','orchestral conducting', 'organ performance',
                          'painting', 'percussion', 'performing arts', 'fashion',
                          'fashion design','film','film and television','film directing',
                          'fine art','fine arts','flute','flute performance', 'graphic art',
                          'graphic design', 'illustration','industrial design','interactive media',
                          'media art', 'modern art','photography', 'piano','piano performance',
                          'poetry','product design', 'publishing', 'recording arts','screen writing',
                          'screenwriting','sculpture','studio art','studio arts','theater',
                          'theater arts','theatre','visual art','visual effects','vocal performance',
                          'voice','voice and opera', 'apparel design', 'animation','acting',
                          
                          ],
                  
                  'humanities':['chicano studies' 'italian studies', 'latin american studies',
                               'japaneese studdies', 'chinese studies','near eastern studies',
                               'near and middle eastern studies', 'diversity studies',
                               'east asian studies','hebrew', 'english', 'portuguese',
                               'italian', 'italian language', 'italian language and literature',
                               'italian studies','japaneese', 'japaneese language',
                               'japanese language and literature', 'japanese studies', 'latin', 
                               'bulgarian language','latin american and caribbean studies',
                               'chinese', 'chinese language', 
                               'chinese language and culture','english',
                               'english as a second language','english language',
                               'english language and literature',
                               'english language arts','english literature',
                               'english studies','mandarin','liberal arts', 'literature', 
                               'classical languages', 'classical studies', 'classics',  'creative writing',
                               'czech language', 'danish language', 'european history','european studies',
                               'foreign studies','french','french language','french language and literature',
                               'french literature','french studies','gender studies''german',
                               'german language','german language and culture',
                               'german language and literature','german language studies',
                               'german studies','global studies','greek','greek language',
                               'greek mythology','hebrew','humanities', 'journalism', 'liberal arts',
                               'liberal studies','literature','mandarin','mandarin chinese',
                               'mandarin studies', 'middle eastern studies','modern languages',
                               'multimedia','multimedia journalism','near and middle eastern studies',
                               'near eastern studies','russian','russian language',
                               'russian language and culture','russian language and literature',
                               'russian studies', 'spanish','spanish culture','spanish language',
                               'spanish language and culture','spanish language and literature',
                               'spanish language studies','spanish literature','spanish studies',
                               'swedish','swedish language','ukrainian','ukrainian language',
                               'writing', 'writing and culture', 'british literature',
                               'asian studies', 'art history','art therapy', 'architecture',
                               'art','art and design','arabic','arabic language',
                               'arabic language and literature', 'american history',
                               'american indian studies','american literature',
                               'american sign language','american studies',
                                'african american studies','african studies',
                                'africana studies',
                               ],
                  
                  'religion':['christian administration','divinity','ministry','rabbinic studies',
                              'rabbinical studies','religious education','religious studies',
                              'theological studies','theology','theology and religious vocations',
                               'buddhist studies', 'biblical studies',
                              ],
                  
                  'stem':['math', 'mathematics', 'applied math',
                          'chemistry', 
                          'electrical engineering', 'mechanical engineereing', 
                          'chemical engineering', 'microbiology',
                          'petrolium engineering',  'civil engineering',
                          'comp sci', 'computer','computer and information science',
                          'computer and information systems','computer application',
                          'computer applications','computer engineering',
                          'computer graphics','computer information systems',
                          'computer programming','computer science',
                          'computer science and engineering',
                          'computer science and mathematics','computer systems',
                          'computer systems engineering','computer systems management',
                          'computer tech', 
                          'cybersecurity', 'data science',
                          'electrical engineering and computer science','electronics',
                          'electronics engineering','engineering','engineering tech',
                          'game design','general engineering','genetics', 'genomics',
                          'industrial and manufacturing engineering','industrial engineering',
                          'informatics','marine biology','marine science','mathematical science',
                          'mechanical engineering', 'mechanical tech','mechatronics',
                          'medical engineering','metallurgical engineering', 'mining engineering',
                          'molecular biology','molecular biophysics','nanotechnology',
                          'network administration','neuroscience','nuclear engineering',
                          'organic chemistry', 'petroleum engineering','physical science',
                          'physics','physiology','robotics','software development',
                          'software engineering','statistics','toxicology', 'botany',
                          'biotechnology', 'biochemical engineering','biochemistry',
                          'bioengineering','bioinformatics','biological engineering',
                          'biological science','biology','biomedical engineering',
                          'biomedical science','biophysics', 'automotive mechanics',
                          'aviation','avionic systems','avionics','avionics maintenance',
                          'astronomy','astrophysics', 'architectural engineering',
                          'aquaculture', 'applied economics','applied mathematics',
                          'applied physics','applied psychology','applied science',
                          'applied statistics', 'agricultural engineering',
                          'aerospace engineering', 'actuarial science',
                          
                          ],
                  
                  'science_not_stem':['entomology','construction','construction management',
                                      'ecology','environmental engineering','environmental science',
                                      'environmental studies','food safety','food science',
                                      'forest science','forestry','general science','horticulture',
                                       'materials science','meteorology','mining','natural resource conservation',
                                       'natural resources conservation','natural resources management',
                                       'natural science','naval architecture','naval architecture and marine engineering',
                                       'oceanography','natural resource conservation', 'science information',
                                       'soil science','wildlife and fisheries science','zoology',
                                        'animal science','animal behavior', 'agronomy','agriculture',
                                      ],
                  
                  #Need help getting  the differnece between social sciences, liberal arts and humanitites and other 
                  #Will probably reach out to fred to help me with this, he's actually good at 
                  #stuff like this, and it's not like he works anyway, so this could be good 
                  #on the other hand it would mean we would have to pay him, which is a negative
                  
                  'social_science':['economics', 'linguistics', 
                                    'family', 'family and community services',
                                    'family and consumer science','family science','family therapy',
                                    'child and adolescent development', 'child development',
                                     'communication','communication management','communication studies',
                                     'communications',
                                     'community development','consumer science', 'counseling',
                                     'counseling psychology','sociology', 'earth science',
                                     'entrepreneurial studies', 'family','family and community services',
                                     'family and consumer science','family science','family therapy',
                                     'geography','geology','geophysics','geoscience','geosciences',
                                     'geotechnical engineering','global health','government',
                                     'government and politics','historic preservation','history',
                                     'human development','industrial and organizational psychology',
                                     'interdisciplinary social science','international affairs',
                                     'international development','international economics',
                                     'international relations', 'logic','media management',
                                     'media studies','metaphysics','meteorology', 'military science',
                                     'mythology','occupational health','occupational health and safety',
                                     'occupational safety','occupational therapy','philosophy',
                                     'police science','political science','political science and government',
                                     'politics','poly sci','project management','psychology','rhetoric',
                                     'social psychology','social science','social sciences',
                                     'social studies','sociology', 'speech and hearing science',
                                     'speech communications','speech language pathology','speech pathology',
                                     'speech science','urban planning','urban studies', 'behavioral science',
                                     'archeology', 'anthropology', 'agricultural economics',
                                     'addiction counseling',

                                    ],
                  
                  'liberal_arts':['organization administration','organizational communications',
                                   'organizational leadership','organizational management',
                                   'communications and media studies','parks recreation and leisure studies',
                                   'public admin','public administration','public affairs','public health',
                                   'public relations','public service','public service and administration',
                                   'quality improvement','radio broadcasting','real estate','regional planning',
                                   'social services', 'social work','sustainability','sustainability management',
                                   'sustainability studies','telecommunications',
                                   'television','television broadcasting','tourism','tourism management',
                                   'travel','travel and tourism management','travel services management',
                                   'visual communications','web development', 'broadcasting','broadcast journalism',
                                   'audiology', 'advertising',                              
                                  ],
                  
                  
                  
                  'health_care':['optometry', 'pharmaceutical science',
                                 'pharmacology', 'pharmacy',  'clinical nurse',  'clinical nursing',
                                 'clinical psychology', 'dental hygiene','dentistry',
                                 'epidemiology', 'gynecology','health administration',
                                 'health and medical administrative services','health care',
                                 'health care administration','health science',
                                 'health service administration','health services',
                                 'healthcare','healthcare administration','health promotion',
                                 'immunology','exercise and sport science','exercise science',
                                 'kinesiology','medical biology','medical engineering','medical insurance',
                                 'medicine', 'nursing','nursing administration','nutrition',
                                 'nutrition science','orthodontics','pharmaceutical science',
                                 'pharmacology','pharmacy','physical therapy','physiotherapy',
                                 'pre optometry','premed', 'radiology','toxicology',
                                 'veterinary studies','virology', 'anatomy','anesthesiology',
                                 
                                 ],
                  
                  'legal':['law', 'paralegal','paralegal studies', 'prelaw','corporate law','court reporting',
                          'criminal justice', 'criminology', 'ethics','forensic science',
                          'forensics','homeland security', 'international law','law enforcement',
                          'legal administration','legal studies','public policy','trial advocacy',
                           'administration of justice',
                          ],
                  
                  
                  
                  'other':[ 'cognitive science','college student affairs',
                          'commerce',  'conflict resolution',  'corrections','customer service',
                           'distribution management','energy','event planning','fundraising',
                           'general education', 'general studies','hospitality',
                           'hospitality and tourism','hospitality management','hotel management',
                           'industrial administration','industrial relations','industrial safety',
                           'interdisciplinary studies','labor relations','landscape architecture',
                           'leadership','library science', 'management','management science',
                           'mass media','operations management','retail','sommelier','vocational rehabilitation',
                           'welding', 'apparel and textiles', 'administration',
                           'cosmetology',
                            'coaching','sport administration','sport management',
                            'sports management',],
                  
                  'teaching':['curriculum and instruction', 'curriculum supervision',
                              'early childhood education','education',
                              'educational administration and supervision',
                              'educational psychology','elementary education','health education',
                              'music education','physical education','religious studies',
                              'special education','teacher education','teaching', 'art education',                
                              
                              ],
        }
    
    #getting a dataframe with columns of each major 
    m = pd.get_dummies(df.majors_ls.apply(pd.Series).stack()).sum(level = 0)
    
    #creating columns for each dicipline 
    df['stem'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['stem']) & set(m.columns.tolist()))]), axis = 1)
    df['business'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['business']) & set(m.columns.tolist()))]), axis = 1)
    df['arts'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['arts']) & set(m.columns.tolist()))]), axis = 1)
    df['social_science'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['social_science']) & set(m.columns.tolist()))]), axis = 1)
    df['liberal_arts'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['liberal_arts']) & set(m.columns.tolist()))]), axis = 1)
    df['health_care'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['health_care']) & set(m.columns.tolist()))]), axis = 1)
    df['legal'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['legal']) & set(m.columns.tolist()))]), axis = 1)
    df['other'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['other']) & set(m.columns.tolist()))]), axis = 1)
    df['teaching'] = m.apply(lambda x: sum([x[major] for major in list(set(diciplines['teaching']) & set(m.columns.tolist()))]), axis = 1)
    
    #filling nulls 
    df['stem'] = df.stem.fillna(0)
    df['business'] = df.business.fillna(0)
    df['arts'] = df.arts.fillna(0)
    df['social_science'] = df.social_science.fillna(0)
    df['liberal_arts'] = df.liberal_arts.fillna(0)
    df['health_care'] = df.health_care.fillna(0)
    df['legal'] = df.legal.fillna(0)
    df['other'] = df.other.fillna(0)
    df['teaching'] = df.teaching.fillna(0)
    
    #it looks like there are people wth more than 1 degree in a certain field, but I don't want this to be a continuous field 
    #but I want it to be a categorical so i am going to make someone with a 1 or 0 
    
    df['stem'] = df['stem'].apply(lambda x: 1 if x >= 1 else 0)
    df['business'] = df['business'].apply(lambda x: 1 if x >= 1 else 0)
    df['arts'] = df['arts'].apply(lambda x: 1 if x >= 1 else 0)
    df['social_science'] = df['social_science'].apply(lambda x: 1 if x >= 1 else 0)
    df['liberal_arts'] = df['liberal_arts'].apply(lambda x: 1 if x >= 1 else 0)
    df['health_care'] = df['health_care'].apply(lambda x: 1 if x >= 1 else 0)
    df['legal'] = df['legal'].apply(lambda x: 1 if x >= 1 else 0)
    df['other'] = df['other'].apply(lambda x: 1 if x >= 1 else 0)
    df['teaching'] = df['teaching'].apply(lambda x: 1 if x >= 1 else 0)
    
    return df 

def get_languages(df):
    #creating a dictionary of titles 
    langs = [{'asian':['chineese', 'mandrin', 'cantonese', 'korean', 'japanese',
           'himachal pradesh', 'delhi', 'haryana', 'uttar  pardesh', 
           'madhya pardesh', 'bihar', 'uttaranchal', 'jharkhand', 
           'rajasthan', 'chattisgarh', 'malayalam', 'tamil','telugu',
           'vietnamese', 'thai', 'indonesian', 'tagolag', 'malay', 'cambodian',
           'nepalese', 'sri lankan', ],
           
           'pacific':['samoan', 'tongan', 'fijian', 'tahitian', 'hawaiian'],
           
           'latin':['spanish', 'portugese'],
           
           'african':['hatian creole', 'jamacian'],
           
           'european':['urdu', 'ojibwa', 'french']
           }]
    return df 




def get_interest_groups(df):
    groups = {
        'art_interest':['3d printing', 'all music', 'all things business',
               'all things business and marketing', 'art', 'arts and crafts',
               'arts and culture', 'ashleigh brown photography',
               'astro imaging', 'canvasser',  'color theory', 
               'community theater','ballroom dancing', 'competitive ballroom dancing',
               'contact improv','content creator', 'culture', 'developing darkroom photography',
               'digital and film photography', 'digital media', 'digital photography',
               'drawing', 'documentary photography', 'jewelry making', 'korean',
               'korean culture', 'love live music','media', 'painting', 'pencil drawings',
               'pencil sketching', 'photo editing','photo retouching','photograph',
               'photography','photoshop', 
               
               ],
        
        'music_interest':['7 kiis fm', 'arabic music', 'brass instruments', 
                'brass instruments','classic rock',
                'classical music','classical music and opera', 'guitar', 'guitar and traveling',
                'guitar weightlifting',  'indian music','ital vibes', 'internet music radio',
                'listening music', 'listening to music','movies','movies and tv',
                'music','music (guitar/piano', 'play the piano',
                'piano enthusiast and to be novelist','playing piano','piano',
                'playing the keyboard',
                 
               ],
        
        'sports_interest':['5k a plus','aerobics', 'also assistant baseball coach',
                   "amar'e stoudemire",'andre iguodala', 'badmintion','badminton',
                   'basketball', 'being active','biking', 'boating', 'boston sports',
                   'capoeira', 'college basketball', 'competitive showjumping',
                   'cycling', 'downhill skiing', 'environment', 'exercise', 
                   'fantasy baseball','fantasy sports', 'fishing and vacationing with the family',
                   'fishing','fishinggolfcooking', 'fitness', 'fly fishing', 'football',
                   'football (soccer', 'gator football', 'golf', 'guitar weightlifting',
                   'hockeybaseballfootballsports analytics','hockey','baseball',
                   'football', 'sports analytics', 'i am a multi marathon finisher',
                   'i enjoy snowboarding', 'ice hockey referee',
                   'indoor rock climbing/bouldering', 'jogging', 'keeps fit lap swimming and bicycling',
                   'biking', 'bicycling', 'swimming',  'kick boxing', 'kickboxing',
                   'ku jayhawks','miami heat basketball', 'mixed martial arts',
                   'mixed martial arts (gracie jiu jitsu', 'my interests include running',
                   'nba', 'new york road runners', 'open water swimming', 'p90x',
                   'personal fitness', 'philosophysports','sports', 'playing/watching soccer',
                   
                   ],
        
        'other_sciences_interest':[ 'aerosol science', 'aviation', 'climate', 
                                    'clinical pharmacy', 'metabolic engineering',
                                     'pacific biosciences',
                    ],
        
        'board_games_interest':['agricola', 'chess',],
        
        'charity_interest':[ "alzheimer's walk committee", 'animal welfare',
                             'civil rights and social action', 'community service',
                             'disaster and humanitarian relief', 'equity research',
                             'fund raising', 'fundraising and development', 
                             'helping people get hired','helping those in need',
                              'international aid',
                             ],
        
        'social_sciences_interest':[ 'european history','economic empowerment',
                                    'economics', 'feminist philosophy',
                                    'gender violence/ gender equality', 'health economics',
                                    'human rights','languages', 'logic', 'philosophysports',
                                    ],
        
        'stem_interest':['actuary exam', 'artificial intelligence', 'aerospace',
                'applied econometrics', 'astrological analysis(a bit','astronomy',
                'big data', 'biomedical engineering','biotechnology', 'comptia network',
                'computer game','computer networking','computer programming',
                'computer science','computers', 'coursera', 'data analysis',
                'data engineering','data mining','data mining and big data analysis',
                'data science','data scientist','data storage','data structures',
                'data warehousing', 'electronics', 'energy efficiency',
                'energy research', 'ensemble learning', 'epidemiology',
                'evolutionary/biological sciences', 'extreme programming',
                'fraud detection and risk analytics', 'functional programming',
                'genetic engineering', 'graph databases', 'hdfs (hadoop distributed file system',
                'it technology', 'latex','learning algorithms', 'learning different programming languages',
                'learning new programming languages','learning new statistical methodology',
                'machine learning','machine learning and data science',
                'machine learning data mining analytics','management of technology',
                'math','mathematical oncology','mathematical optimization','mathematics',
                'mentoring junior programmers', 'molecular biology', 'moonshots',
                'multivariate analysis', 'natural language processing (nlp',
                'natural science and physics', 'networking and network programming',
                'neural networks', 'new technology', 'numerical software',
                'pacific biosciences', 'parallel processing','pharmaceutical sciences',
                'physics', 'playing with data',
                
                
                ],
        
        'travel':[ 'australia','backpacking','baltimore md', 'beach', 'berlin',
                   'foreign travel', 'france', 'i like reading and traveling',
                   'international travel', 'international law', 'international business',
                   'international aid', 'international advertising', 'international advertising',
                    'korean','korean culture', 'muslims', 'national parks', 'new york city',
                     'offshore fishing',
                   ],
        
        'adventure':[ 'adventure', 'australia','backpacking', 'beach', 'berlin',
                      'cannoning','canoeing (coach', 'caving', 'enjoy the nature outside',
                      'enjoying camping', 'foreign travel', 'france', 'hiking',
                      'hiking and all things outdoors','hiking/camping', 'kayaking',
                      'kayaking (anything outdoors', 'mountain trekking','mountaineering'
                      'mountain trecking', 'mountains', 'national parks', 'new york city',
                      'offshore fishing', 'outdoor activities','outdoor advertising',
                      'outdoors',
                      ],
                      
        
        #come and put every nba team and probs every nfl team 
        'basketball':[ "amar'e stoudemire",'andre iguodala', 'basketball',
                'college basketball', 'ku jayhawks','miami heat basketball',
                 'nba',],
        
        'cooking':[ 'baking','baking bread and pizza', 'burrito enthusiast',
                   'chocolate chip cookies', 'coffee', 'cooking','cooking and food in general',
                   'cooking/baking', 'culinary arts', 'eating', 'gourmet cooking',
                    'i love cooking and baking',
                   ],
        
        'dancing':[ 'ballroom dancing', 'competitive ballroom dancing','dance',
                   'dance (ballet','dancing',],
        
        
        'writing_interest':['blog writing','blogging','blogging in newspapers',
                            'books', 'cnbc', 'co ops and intentional communities',
                            'comedy', 'i love working as a writing consultant',
                            'i love writing', 'journalism', 'kindle', 'newspaper', 'poetry',
                            ],
        
        'business_interest':[ 'business creation', 'business management','business strategy',
                    'business/entrepreneurial ideas', 'capital market','capital markets',
                    'career','career growth', 'consulting', 'entrepreneurship',
                    'financial analysis','financial industry and investing',
                    'hris management', 'international business', 'investing',
                    'marketing','marketing strategies','mobile marketing',
                    'negotiation strategy','networking', 'online advertising',
                    'online marketing and graphic design','online/social media',
                    
                      ],
        
        'steryotypical_male_interests':[ 'casinos','artificial intelligence', 'aerospace',
                'applied econometrics',"amar'e stoudemire",'andre iguodala', 'college basketball',
                 'dc comics', 'electronics and time with my wife', 'fantasy baseball',
                 'fantasy sports', 'fishing and vacationing with the family','fishing',
                 'golf',  'golfing', 'game development','game programming', 'gator football',
                 'gillette', 'motorcycle riding','motorcycling',
                 'motorcyclingsnowboardingkaratevolleyball',
                 
                ],
        
        'steryotypical_female_interest':['children', 'celebrities', 'event planning',
                                         'fashion', 'feminist philosophy', 'figure skating',
                                         'grandkids','jewelry making', 'kids',
                                         'kinderen',
                                          ],
        
        'religious_interest':['christianity', 'church accounting', 'muslims',],
        
        'foreign_interest':[ 'ciencia y tecnología', 'ciência e tecnologia',],
        
        'steryotypical_upperclas_interest':['competitive showjumping', 'downhill skiing',
                                            'figure skating', 'horses', 'i enjoy snowboarding',
                                            ],
        
        'web_dev_interest':[ 'basic html 5', 'cool web apps', 'java', 'it technology',
                             'internet',],
        
        'legal_interest':[ 'copywriting/content development', 'federal criminal defense',
                           'international law', 'law', 'model united nations',],
        
        'health_care_interest':[ 'health','health care', 'hospital pharmacy',
                                 'medical research','medicine','pharmaceutical sciences',
                                 ],
        
        'soft_skills':['leadership','leadership education','leadership training',
                      'learning', 'life potentials coaching', 'my interests include my kids',],
        
        'family':[ 'children', 'electronics and time with my wife', 'kids',
                  'grandkids', 'fishing and vacationing with the family',
                  'my interests include my kids','kinderen',]
                                                    
        }

    return df 

def get_cols(df):
    print("getting countries")  
    df = prev_countries(df)
    print('getting degrees')
    df = get_degrees(df)
    print('getting edu levels')
    df = get_edu_level(df)
    print("getting summary length")
    df = get_len_summary(df)
    print("getting pdl gender")
    df = get_pdl_gender(df)
    print('getting major dicipline')
    df = get_major_dicipline(df)
    print("getting langauages")
    #data3 = get_langiages(data3)
    print('done')
    return df 

data3 = get_cols(data3)


# =============================================================================
'''creating visuals for brad and my own underestanding '''
def graph_ethnic_groups(df):
    
    vals = [sum(df.asian)/len(df), sum(df.european)/len(df),
            sum(df.pacific)/len(df), sum(df.latin)/len(df), sum(df.african)/len(df)]
    
    groups = ['asian', 'european', 'pacific', 'latin', 'african']
    
    plt.subplot(1, 3, 1)
    ax = sns.barplot(x = vals, y =groups )
    plt.title("Ethic Breakdown of Population")
    for p in ax.patches:
        percentage ='{:.2f}'.format(p.get_width())
        width, height =p.get_width(),p.get_height()
        x=p.get_x()+width+0.01
        y=p.get_y()+height/2
        ax.annotate(percentage,(x,y))
    plt.xlim((0, 1))
    plt.show()
'''
ok rather surprising, I thought that given the diversity of california, texas
and florida I thought that there would be a lot more people of african and latin 
decent, sooo that's interesting
'''
#-------------------

def graph_edu_level(df):
    '''this does function does not show the highest degree a person has, it just 
    shows the education level an individual has attained, we do not double count 
    for people who have two masters degrees for example, we only count that they
    have a masters. 
    '''
    vals = [sum(df.associates)/len(df), sum(df.bachelors)/len(df),
            sum(df.masters)/len(df), sum(df.phd)/len(df)]
    groups = ['associates', 'bachelors', 'masters', 'phd']
    
    plt.subplot(1, 3, 1)
    ax = sns.barplot(x = vals, y =groups )
    plt.title("Education Breakdown")
    for p in ax.patches:
        percentage ='{:.2f}'.format(p.get_width())
        width, height =p.get_width(),p.get_height()
        x=p.get_x()+width+0.01
        y=p.get_y()+height/2
        ax.annotate(percentage,(x,y))
    plt.xlim((0, 1))  
    plt.show()

#------------------
    
def graph_summary_len_dist(df):
    data = df.sum_length
    plt.hist(data, color = 'skyblue')
    plt.title('distribution of summary length')
    plt.show()
    
#--------------------
def graph_pdl_gender(df):
    vals = [sum(df.pdl_male)/len(df), sum(df.pdl_female)/len(df), 
            sum(df.no_pdl_gender)/len(df)]
    gender = ["Male", "Female", "NA"]
    ax = sns.barplot(x = vals, y =gender )
    plt.title("PDL Gender Breakdown")
    for p in ax.patches:
        percentage ='{:.2f}'.format(p.get_width())
        width, height =p.get_width(),p.get_height()
        x=p.get_x()+width+0.01
        y=p.get_y()+height/2
        ax.annotate(percentage,(x,y))
    plt.xlim((0, 1))
    plt.show()
    
#----------------
def graph_major_disciplines(df):
    vals = [sum(df.stem)/len(df), sum(df.business)/len(df), sum(df.arts)/len(df),
            sum(df.social_science)/len(df), sum(df.liberal_arts)/len(df),
            sum(df.health_care)/len(df), sum(df.legal)/len(df), 
            sum(df.other)/len(df), sum(df.sports)/len(df), 
            sum(df.teaching)/len(df), 
            ]
    gender = ['stem', 'business', 'arts', 'social sciences', 'liberal arts',
              'health care', 'legal', 'other', 'sports', 'teaching']
    
    ax = sns.barplot(x = vals, y =gender )
    plt.title("Major Dicipline Breakdown")
    for p in ax.patches:
        percentage ='{:.2f}'.format(p.get_width())
        width, height =p.get_width(),p.get_height()
        x=p.get_x()+width+0.01
        y=p.get_y()+height/2
        ax.annotate(percentage,(x,y))
    plt.xlim((0, 1))
    plt.show()
    

print("graphing")
graph_ethnic_groups(data3)
graph_edu_level(data3)
graph_summary_len_dist(data3)
graph_pdl_gender(data3)
graph_major_disciplines(data3)

# =============================================================================






    