#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug 19 15:46:49 2020

@author: a13855
"""
'''
'Anthropology':{ ['Anthropology and Outline of anthropology', 'Anthropological criminology',
'Anthropological linguistics', 'Synchronic linguistics', 'Diachronic linguistics', 'Ethnolinguistics',
'Semiotic anthropology', 'Sociolinguistics', 'Anthrozoology', 'Biological anthropology',
'Gene-culture coevolution', 'Evolutionary anthropology', 'Forensic anthropology',
'Human behavioral ecology', 'Human evolution', 'Medical anthropology', 'Molecular anthropology',
'Neuroanthropology', 'Nutritional anthropology', 'Paleoanthropology', 'Population genetics',
'Primatology', 'Biocultural anthropology', 'Cultural anthropology', 'Anthropology of development'
'Anthropology of religion', 'Applied anthropology', 'Cognitive anthropology', 
'Cyborg anthropology', 'Digital anthropology', 'Digital culture', 'Ecological anthropology',
'Economic anthropology', 'Environmental anthropology', 'Ethnobiology', 'Ethnobotany',
'Ethnography', 'Ethnohistory', 'Ethnology', 'Ethnomuseology', 'Ethnomusicology',
'Feminist anthropology', 'Folklore', 'Kinship', 'Legal anthropology', 'Mythology',
'Missiology', 'Political anthropology', 'Political economic anthropology',
'Psychological anthropology', 'Public anthropology', 'Symbolic anthropology',
'Transpersonal anthropology', 'Urban anthropology', 'Linguistic anthropology',
'Social anthropology', 'Anthropology of art', 'Anthropology of institutions',
'Anthropology of media', 'Visual anthropology'
]
}
'''

import pandas as pd 
labeled = pd.read_csv("/Users/a13855/Desktop/OrderBoard/div_inc/data/Orderboard_Profile_Info.csv")

def main(csv_path, index_name, output_path):
	df = pd.read_csv(csv_path)
	ls_list = df[index_name].tolist()
	file1 = open(output_path, "w")  # append mode
	file1.write("{\n")
	file1.write('  "query": {\n')
	file1.write('    "bool": {\n')
	file1.write('      "must": [\n')
	file1.write('        {\n')
	file1.write('          "terms": {\n')
	file1.write('            "id.keyword": [\n')
	for item in ls_list:
		if item != ls_list[-1]:
			file1.write(f'              "{item}",\n')
		else:
			file1.write(f'              "{item}"\n')
	file1.write('            ]\n')
	file1.write('          }\n')
	file1.write('        }\n')
	file1.write('      ]\n')
	file1.write('    }\n')
	file1.write('  }\n')
	file1.write('}\n')

	file1.close()


if __name__ == '__main__':
	csv_path = "/Users/a13855/Desktop/OrderBoard/div_inc/data/Orderboard_Profile_Info.csv"
	index_name = 'id'
	output_path = "/Users/a13855/Desktop/OrderBoard/div_inc/data/query_for_people.txt"
	main(csv_path, index_name, output_path)
    
